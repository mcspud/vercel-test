## Getting Started

Project has been set up with a local Postgres database.  

```bash
docker compose up
```

This will create a Postgres server on localhost attached to `5432`.

Next run the development command to set up the local database.  Since this application uses SSR and the homepage gets database records you wont be able to run the application until you do this.

```bash
npm run migrate:dev 
```

After that run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Flow

1. When a user goes to `/` they will see a list of users.  These are served from a postgres database.
2. A `new` user will not be able to edit their profile or select the `information` link.
3. A `new` user should hit the "Register" button and create a new user.  The only constraint is unique email addresses.  The list of users will update immediately showing the new user added to the head of the list.
4. Once a user has been created in the database you will be able to modify your profile.
5. Once a user has been created in the database you will be able to access the "Information" page.  This page uses a GraphQL query that returns a list of space objects.
6. To "log out" refresh the page, and go back to being in a `new user` state.

## Implementation 

Besides the compiler, framework and style libraries, only 3 external libraries have been used:

1. Zod, for data validation
2. Drizzle, for SQL including migrations.
3. `ts-node` to run the migrations

The rest of it uses the capabilities of Nextjs and Vercel as much as possible. 


## Deployment 

A terraform project has been set up to deploy the environment.  You will need a valid terraform token in your shell.

```bash
cd infrastructure
terraform apply
> yes
```

This will deploy an environment.  At the time of writing this [there is no way to provision a Vercel postgres](https://github.com/vercel/terraform-provider-vercel/issues/127) database via the terraform provider, as the vercel REST endpoints have not exposed this resource.  You will need to:

- create a storage unit via the web console
- link the projects together via the web console
- redeploy the application
