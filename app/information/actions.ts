import { ApolloClient, InMemoryCache, ApolloProvider, gql } from '@apollo/client';
import z from 'zod'


// Lift the schema out of the getItems() function so we can use it
// for typing elsewhere
const plantSchema = z.object({
  id: z.string().nonempty(),
  name: z.string().nonempty(),
  description: z.string().nonempty(),
  photo: z.string().nonempty(),
})
// Generate type from zod function
export type PlantT = z.infer<typeof plantSchema>

// Does a graphql call do a random endpoint.  It runs the results through Zod
// to make sure the incoming types are actually correct, rather than us blindly
// trusting remote GraphQL operations.
export async function getItems() {
  const client = new ApolloClient({
    uri: 'https://flyby-router-demo.herokuapp.com/',
    cache: new InMemoryCache(),
  });

  const schema = z.array(plantSchema)

  try {
    const response = await client
      .query({
        query: gql`
          query GetLocations {
            locations {
              id
              name
              description
              photo
            }
          }
    `,
      })

    const data = schema.parse(response.data.locations)
    return { success: true, data: data }
  } catch (e) {
    console.log(e)
    // Returns an empty list as a monoid
    return { success: false, data: [] }
  }
}
