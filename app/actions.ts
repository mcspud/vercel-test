"use server"

import zod from 'zod'
import db from '@/lib/db'
import { eq, count, desc } from 'drizzle-orm'
import { UsersTable } from '@/lib/schema'
import { revalidatePath } from 'next/cache'
import { nonSelectedUserId } from './constants'

// This solely exists due to an open bug involving drizzle and nextjs, see bug report here.
// This is also implemented with the "InnerLayout" component so I can escape the drizzle/nextjs
// async race condition
// https://github.com/vercel/next.js/issues/54282
export async function initAction() { }

// returns the details of a user
export async function getUser(id: number) {
  return await db.select().from(UsersTable).where(eq(UsersTable.id, id)).limit(1)
}

// Retrieves a list of users, with most recently added at head of list
export async function listUsers() {
  const data = await db.select().from(UsersTable).orderBy(desc(UsersTable.id))
  revalidatePath('/')
  return data
}

// Retrieves a count of all users
export async function countUsers() {
  const data = await db.select({ value: count() }).from(UsersTable)
  revalidatePath('/')
  return data
}

// Creates a user
export async function createUser(prevState: any, formData: FormData) {
  // Use Zod to ensure the correct data is passed in. 
  const schema = zod.object({
    email: zod.string().nonempty(),
    position: zod.string().nonempty()
  })

  try {
    const data = schema.parse({ email: formData.get('email'), position: formData.get('position') })
    // Since Zod generates types for us we can pass in `data` arbitrarily without
    // having to manually define properties 1-by-1.  This allows us to leverage
    // types to ensure there are no seams between the database layer and the input
    // layer
    const user = await db.insert(UsersTable).values(data).returning({ id: UsersTable.id })
    // refresh the SSR cache so that the `Users` page shows the new entries
    revalidatePath('/')
    return { success: true, key: user[0].id }
  } catch (e) {
    console.log(e)
    return { success: false, key: nonSelectedUserId }
  }
}

// Updates an existing user
export async function updateUser(prevState: any, formData: FormData) {
  // Use Zod to ensure the correct data is passed in.  Negative numbers are invalid primary keys,
  // and so too is "0"
  const schema = zod.object({
    id: zod.coerce.number().nonnegative().gt(0).int(),
    email: zod.string().nonempty(),
    position: zod.string().nonempty()
  })

  try {
    const data = schema.parse({
      id: formData.get('id'),
      email: formData.get('email'),
      position: formData.get('position')
    })

    // Since Zod generates types for us we can pass in `data` arbitrarily without
    // having to manually define properties 1-by-1.  This allows us to leverage
    // types to ensure there are no seams between the database layer and the input
    // layer
    const result = await db.update(UsersTable)
      .set(data)
      .where(eq(UsersTable.id, data.id))
      .returning({ id: UsersTable.id, email: UsersTable.email, position: UsersTable.position })

    // refresh the SSR cache so that the `Users` page shows the new entries
    revalidatePath('/')

    return {success: true}
  } catch (e) {
    console.log(e)
    return {success: false}
  }
}
