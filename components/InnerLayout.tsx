"use client"

import {
  Box,
  useColorModeValue,
  useDisclosure,
} from '@chakra-ui/react'
import { ChakraProvider } from '@chakra-ui/react'
import Header from '@/components/Header'
import SidebarNav from '@/components/SidebarNav'
import MobileNav from '@/components/MobileNav'
import { nonSelectedUserId } from '@/app/constants'
import { createContext, useState } from 'react'
import AddFormModal from './AddFormModal'

// This represents a very simple Context that loads in the primary key of a logged in user.  Normally
// you'd use a much stronger solution for this, by either storing it locally in localStorage/appStorage 
// or a cookie, or storing it in memory with a library like redux.  The challenge said to use as little 
// external libraries as possible so here we go.
export const ActiveUserContext = createContext({
  currentUser: nonSelectedUserId,

  setCurrentUser: (value: number) => { }
})

export default function InnerLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {

  // Needed for mobile navigation
  const sidebar = useDisclosure()
  // Shows an open modal by default when a user logs in.
  const addNewUser = useDisclosure({defaultIsOpen: true})
  // Sets the context relating to the default "saved" user
  const [currentUser, setCurrentUser] = useState(nonSelectedUserId)

  return (
    <ChakraProvider>
      <ActiveUserContext.Provider value={{ currentUser, setCurrentUser }}>
        <Box minH="100vh" bg={useColorModeValue('gray.100', 'gray.900')}>
          <SidebarNav disclosure={sidebar} />
          <MobileNav disclosure={sidebar} />
          <Header onOpen={sidebar.onOpen} />
          <AddFormModal disclosure={addNewUser} />
          <Box ml={{ base: 0, md: 60 }} p="4">
            {children}
          </Box>
        </Box>
      </ActiveUserContext.Provider>
    </ChakraProvider>
  );
}
