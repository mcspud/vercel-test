import type { Config } from "drizzle-kit";
import { loadEnvConfig } from '@next/env'

// This is done to allow the dizzle-kit to automagically use the nextjs environment variables
// loader
const projectDir = process.cwd()
loadEnvConfig(projectDir)


export default {
  schema: "./lib/schema.ts",
  out: "./drizzle",
  driver: 'pg',
  dbCredentials: {
    // user: process.env.POSTGRES_USER,
    // password: process.env.POSTGRES_PASSWORD,
    // host: process.env.POSTGRES_HOST as string,
    // port: process.env.POSTGRES_PORT as unknown as number,
    // database: process.env.POSTGRES_DB as string,
    connectionString: process.env.POSTGRES_URL_NON_POOLING as string
  }
} satisfies Config;
