terraform {
  required_providers {
    vercel = {
      source = "vercel/vercel"
      version = "~> 0.16"
    }
  }
}

resource "vercel_project" "example" {
  name      = "vercel-test"
  framework = "nextjs"
  build_command = "npm run migrate:prod && npm run build"
  git_repository = {
    type = "gitlab"
    repo = "mcspud/vercel-test"
  }
}
